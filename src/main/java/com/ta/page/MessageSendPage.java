package com.ta.page;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class MessageSendPage extends BasePage{

    @FindBy(xpath = "//a[@class = 'link3']")
    WebElement successMessage;

    public MessageSendPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(this.driver, this);
    }

    public boolean isEmailSent() {
        this.waitVisibilityOfElement(100, successMessage);
        return successMessage.isDisplayed();
    }
}
