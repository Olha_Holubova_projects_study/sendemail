package com.ta.page;

import com.ta.model.Email;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class MailPage extends BasePage{

    @FindBy(xpath = "//button[@class='button primary send']")
    WebElement buttonSend;

    @FindBy(name="toFieldInput")
    WebElement inputEmail;

    @FindBy(name="subject")
    WebElement inputTopic;

    @FindBy(id="mce_0_ifr")
    WebElement inputText;

    public MailPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(this.driver, this);
    }

    public MessageSendPage sendMail (Email email) {
        inputEmail.sendKeys(email.getRecipientEmail() + Keys.ENTER);
        inputTopic.sendKeys(email.getTopic() + Keys.ENTER);
        inputText.sendKeys(email.getEmailText() + Keys.ENTER);
        buttonSend.click();
        return new MessageSendPage(driver);
    }

}
