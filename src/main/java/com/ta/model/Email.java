package com.ta.model;

public class Email {
    private String recipientEmail;
    private String topic;
    private String emailText;

    public Email(String recipientEmail, String topic, String emailText) {
        this.recipientEmail = recipientEmail;
        this.topic = topic;
        this.emailText = emailText;
    }

    public String getRecipientEmail() {
        return recipientEmail;
    }

    public String getTopic() {
        return topic;
    }

    public String getEmailText() {
        return emailText;
    }
}
